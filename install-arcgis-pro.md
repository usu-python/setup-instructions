# Install ArcGIS Pro

You should've gotten an invitation to USU's ArcGIS Online (AGOL) at your Aggiemail address. If not, please contact me. ArcGIS Pro uses these accounts for licensing, so you can't run it on your own computer without one.

1. Log in to your USU AGOL account and change your password if you haven't already.
2. Download and install the following software, **in the order listed**. If you are asked to log in during the install process, use your USU AGOL username and password.
    - Please see step 2 [here](https://gitlab.com/wild-6920/spring-2020/python-gis/blob/master/setup-instructions/markdown/3-install-arcgis-pro.md) for the links to the files.
3. Open ArcGIS Pro 2.4.1. You should be asked to log in; use your USU AGOL username and password.
    - If you are not asked to log in, when the program starts you will see a log in option in the upper right corner of the screen. Use this link to log in to your AGOL account.
4. ArcGIS Pro should be ready to go!
