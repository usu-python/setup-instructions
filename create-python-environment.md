# Create a new Python environment for the class

ArcGIS Pro allows you to switch between Python *environments* on a project-by-project basis. We're going to use some Python modules in this class that aren't installed by default, but we don't want to risk breaking the main Python environment because of version incompatibilities, so you're going to create a new environment. This might take a while, depending on the speed of your computer and your Internet connection, so make sure you have some time to let your computer chug away, and also make sure it's plugged in so it doesn't run out of battery. 

I've created a [script](create-env.bat) for you so that you don't have to do much other than wait for it to complete. There are a couple of places in the script where it will stop for a long time and you might think it's hung, but it's probably not. Leave it alone and let it do its thing.

### Important: If you've used conda or Anaconda before

I ran into problems on my computer when doing this because it doesn't seem to honor the channel requests, and it was installing packages from conda-forge, which will NOT work here. You need to make sure that you don't have this channel added or else you'll have a lot of heartache when trying to set this up (unless it's just my computer that's ornery, which I doubt). To check, look for a file called `.condarc` in your profile folder (e.g. `C:\Users\chrisg\.condarc`; note the dot at the beginning of the file name). If you have that file, open it up in a text editor and make sure that the **only** entry under `channels` is `defaults`. If there are any others there, delete them before continuing (you might want to make a note of them so you can add them back later). Here's what my entire file looks like:

```
channels:
  - defaults
ssl_verify: true
```

### Set up the environment

1. Use your Start Menu to open the ArcGIS Pro Python Command Prompt. If the prompt doesn't look something like this, with `arcgispro-py3`, then you opened the wrong thing and the rest of the steps won't work.  
![python command prompt](images/command-prompt.png)

2. Now you need to change to the folder where `create-env.bat` is saved. The screenshot below shows the process, where I've highlighted what I typed in yellow and changes or output that you need to watch for in pink. [[Download `create-env.bat`](create-env.bat)]

3. If `create-env.bat` isn't on the `C:\` drive, you need to change drives first. Do that by typing the drive letter followed immediately by a colon (no space), and then <kbd>Enter</kbd>.

4. Now you can use the `cd` (**C**hange **D**irectory) command to change to the folder that contains `create-env.bat`. Type `cd`, followed by a space, and then by the path to the folder containing the file. Then hit <kbd>Enter</kbd>.

5. Make sure that you're in the correct folder by typing `dir *.bat` and checking to see that `create-env.bat` is in the listed files.

6. Now just type `create-env` and hit <kbd>Enter</kbd> and wait around for it to finish.

![create environment](images/create-env.png)

You'll see a ton of stuff scroll by, and it'll look like it stopped a few times, but it's not done until it tells you it is, like this:

![done](images/done.png)

Now you can use this environment in ArcGIS Pro if you want to, although you may not ever need to. We're going to use it without ever opening ArcGIS. 

### Confirm that ArcGIS Pro can see your new environment

1. If you'd like to see how to use it inside ArcGIS Pro, open the software and then open the settings using the button in the lower left corner.  
![open settings](images/open-settings.png)

2. Select Python from the list on the left side of the Settings screen.  
![Python settings](images/python-settings.png)

3. Hit the Manage Environments button.  
![Manage environments](images/manage-env.png)

4. You should see two environments, the default one called `arcgispro-py3`, and the new `python-class` one you just created. There's no need to select the `python-class` one now; this was only to show you that it exists.  
![Environments](images/envs.png)

### Create a Jupyter shortcut for the environment

The shortcut to open Jupyter Notebook that I gave you in your course materials only works in the lab, not on your computer. My computer automatically created a new shortcut in my Start Menu called "Jupyter Notebook (python-class)", but it opens to my profile folder when I want it to open to my `D:` drive. And to make things even better, if you do something like change its working directory, you've just killed it forever (I won't go into why). 

If you want to keep all of your course materials in your Documents folder, then the shortcut created for you should work. If you want to put your course materials elsewhere, then read on.

Because of a limitation with Windows shortcuts, you can't just make a new shortcut that opens where you want. Instead, you'll need to create a batch file like the one I gave you for the lab. Open a new file in any text editor (Notepad will work), and paste the following code in there, but make the following changes (you can also edit [this file](jupyter-notebook-class.bat)):

1. Change `chrisg` to your username (there are 3 spots you need to do this).
2. Change `D:\classes` (at the end) to whatever folder you want to use.

```
"C:\Program Files\ArcGIS\Pro\bin\Python\cwp.exe" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class\python.exe" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class\Scripts\jupyter-notebook-script.py" "D:\classes"
```

Save the file with a `.bat` extension (e.g. `jupyter-notebook-class.bat`). Put it somewhere that you can find it easily, like your desktop. If you'd rather have a shortcut with a Jupyter icon, then save the `.bat` file somewhere permanent and then follow the next step to create the shortcut.

If you want a shortcut:

1. Navigate to the folder that you want to put it in, and right-click and choose New | Shortcut (right-click on the desktop, if that's where you want it).
2. A wizard will ask for the location of the item. Select the `.bat` file that you created earlier.
3. The next pane of wizard will ask for a shortcut name. You can call it whatever you want.
4. After the wizard has completed, right-click on your new shortcut and choose Properties.
5. Hit the Change Icon button near the bottom of the dialog.
6. Paste `%USERPROFILE%\AppData\Local\ESRI\conda\envs\python-class\Menu\jupyter.ico` in the box next to the Browse button and save your changes.

If you'd like a batch file or shortcut for JupyterLab, follow the same steps but change the batch file code to this (with the same changes as before) [[example file](jupyter-lab-class.bat)]:

```
"C:\Program Files\ArcGIS\Pro\bin\Python\cwp.exe" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class\python.exe" "C:\Users\chrisg\AppData\Local\ESRI\conda\envs\python-class\Scripts\jupyter-lab-script.py" "D:\classes"
```




