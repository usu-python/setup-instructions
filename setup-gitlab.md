# Set up your GitLab account and download class materials

You'll use GitHub Desktop to download each week's materials and upload your homework, but you need to set up your GitLab account first.

First, create an account at [gitlab.com](https://about.gitlab.com/). The class materials are available from GitLab and that's also where you'll turn in your homework. You may have heard of GitHub, which is a site for hosting, collaborating on, and sharing code. GitLab is similar, but I like it better because it's open source and they've made a real effort to make it more of a development platform than just a code hosting repository.

## Instructions

1. Create an account at [gitlab.com](https://about.gitlab.com/)

2. Add yourself to the course project by going to <https://chrisg.pythonanywhere.com/wild-6920> and following the directions. The passphrase is in Canvas (and I'll tell you in class). If this spits out an error, please contact me.

3. Once you've created your account and ran the web tool, you should see two projects when you log into your GitLab account. One of these contains the class materials, and the other one with your username in its name is for your homework. While everyone in the class has access to the class materials project, only you and I have access to your homework project.

4. Now create a folder for this class on your flash drive (or your hard drive if you'll be using your own computer). When you're done with these instructions, this folder will contain two subfolders-- one for course materials and one for your homework. The Git software will keep track of the files in these folders, so you'll use them all semester long. You'll run into problems if you try to move files around or use other locations for your files.

5. Copying the GitLab projects (or *repositories*) to your computer is called *cloning* them. That's what you'll do now.

6. Open GitHub Desktop and select the button to Clone a repository.  
![clone](images/github-clone.png)

7. Select the URL tab in the dialog that opens, and paste `https://gitlab.com/wild-6920/spring-2020/python-gis.git` in the Repository URL box.

8. Use the Choose button to select your class folder you created in step 4. The Local Path will automatically add `python-gis` to the end. The `python-gis` folder must not already exist.  
![url](images/github-url.png)

9. Now hit the Clone button. It'll ask for your **GitLab** username and password. The window that opens when it's done won't look like much at this point, but if you hit the Show in Explorer button it'll open your new `python-gis` folder and you can see what's there.  
![finish](images/github-finish.png)

10. Now you need to clone your homework project, so go to that project on [gitlab.com](https://about.gitlab.com/) and find the project's URL. Use the blue Clone button to get a dropdown list with two URLs-- you want the HTTPS one. You can use the circled button to copy it to your clipboard.  
![homework url](images/repo-url.png)

11. Go back to GitHub Desktop and select Clone Repository from the File menu. Paste your homework URL in the Repository URL box, and leave everything else the same as before. After you hit the Clone button a second subfolder will be created for you. This is where you'll put your completed homework assignments.

Congratulations! Now you've got access to the class materials and have a place to store your completed homework. You'll see how to use this to download updated materials and turn in your homework when the time comes, but for now you have the materials for the first week of class downloaded to your `python-gis` folder.
