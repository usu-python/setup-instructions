# Turn in homework

You'll turn in homework by *pushing* it to your personal homework repository on gitlab.com. You can push files as often as you want, and I'll just look at the last version that was uploaded before the due date. You can also push different parts of the homework assignment at different times-- you don't have to do it all at once. Since the homework assignments are usually several problems, a lot of students choose to push each one as they finish it.

1. Make sure that the files you want to turn in are in the appropriate week's subfolder of your homework folder. You can have other files in your homework folder, too, but please only turn in the ones that I ask for. Otherwise it can make it hard for me to sort through all of the extra stuff in your repository-- and that makes me grumpy!

2. Open GitHub Desktop. It'll probably load the last repository you had open, which may or may not be the one you want to use. Check the upper left corner to see which repository it has open, and use the dropdown list to select your homework repo if needed.  
![wrong repo](images/github-wrong-repo.png)

3. Once your homework repository is loaded, you'll see a list of the files in your homework folder that have changed since the last time you committed anything. In this case, it's showing me two files from the first homework assignment from another class (I didn't feel like redoing the screenshots).  
![dirty](images/github-changed-files.png)

4. Make sure that the checkboxes next to the files you want to turn in are checked. Don't check any that you don't want to turn in. *Make sure you don't accidentally highlight any lines in the file, because if you, they're the only ones that will be committed and turned in.*  
![staged lines](images/github-staged-lines.png)

5. Add a short message in the Summary box. Make the message something meaningful. Specifying the homework assignment is good, but you can more details if you want-- just keep it short. If you do want to add something longer, use the Description box.  
![commit](images/github-commit.png)

6. Hit the Commit to master button. This takes a snapshot of the checked files, and all of the information for those files will disappear. This means that they don't have changes since the last commit (which you just did). You can see a list of the commits (snapshots) if you click on the History tab.

7. Now there'll be an option to *push* your committed changes to GitLab. The number 1 means that there is one commit that hasn't been pushed yet. Go ahead and hit the Push origin button. **Your files are not turned in until you Push them to the server.** *(If the button says Fetch instead, then hit it and see what happens. It'll either turn to Pull or Push. If it says Pull, that means that either you or I have put something in your homework folder and it will make you download those files before you can push the new ones. Once you've hit Pull, then it'll change to Push. Once it says Push, then you can use it to upload your files.)*  
![commit](images/github-push.png)

8. After you've pushed the files, you can log in to your GitLab account and look in your homework repository to verify that the files are there.  
![gitlab](images/gitlab-files.png)

### Viewing changes to specific files

This is what GitHub Desktop showed after I added `screenshot.png` to my homework folder and changed a few lines in `test_setup.py`. 

- There's a green plus icon next to the screenshot filename, just like in the earlier examples. This means that the file is new and Git isn't tracking it yet. 
- The yellow icon next to `test_setup.py` means that the file has been modified since the last time I committed. If I click on the filename it even shows which lines I changed (red is what it used to be, and green is what it is now). 

![gitlab](images/github-modified.png) 

I would turn these in by keeping both boxes checked, adding a message, hitting the Commit button and then the Push button, just as before. If I unchecked the box next to `test_setup.py`, then those changes would not be committed and uploaded to GitLab (they'd still be on my computer, though).


